.. _furtherDocumentation:

Further documentation
=======================

.. _synoptic_manual:

The qmesh synoptic manual
-------------------------

The `qmesh synoptic manual <https://qmesh-synoptic-manual.readthedocs.io>_` provides an
overview of the qmesh suite. It is the ideal starting point for newcomers to qmesh, or those
who want to evaluate qmesh, and assess whether is is suitable to their needs.
The `qmesh synoptic manual <https://qmesh-synoptic-manual.readthedocs.io>_` includes examples
on how to use qmesh as well as GIS and other utilities that facilitate mesh generation in
geophysical domains.

You can access the
`qmesh synoptic manual via Read the Docs <https://qmesh-synoptic-manual.readthedocs.io>_`, or
view its source in the
`qmesh development repositories <https://bitbucket.org/qmesh-developers/qmesh-synoptic-manual>_`.

.. _API_Reference:

API Rerefence
-------------------------

CLI Rerefence
-------------------------

GUI Rerefence
-------------------------

.. _academic_papers:

Academic papers
-------------------------

Papers on qmesh
~~~~~~~~~~~~~~~~~~~~~~~~
* Avdis, A. & Candy A. S. & Hill J. & Kramer C. S. & Piggott M. D.,
  *"Efficient unstructured mesh generation for marine renewable energy applications"*,
  Renewable Energy, Volume 116, Part A, February 2018, Pages 842-856,
  `DOI:10.1016/j.renene.2017.09.058 <https://doi.org/10.1016/j.renene.2017.09.058>`_

* Jacobs C. T. & Avdis A. & Mouradian S. L. & Piggott M. D.,
  *"Integrating Research Data Management into Geographical Information Systems"*,
  5th International Workshop on Semantic Digital Archives (SDA), 2015,
  `<http://hdl.handle.net/10044/1/28557>`_, `<http://ceur-ws.org/Vol-1529/>`_

Papers using qmesh
~~~~~~~~~~~~~~~~~~~~~~~~

* Collins D. S. & Avdis A. & Allison P. A. & Johnson H. D. & Hill J. & Piggott M. D., Hassan M. H. A. & Damit A. R.,
  *"Tidal dynamics and mangrove carbon sequestration during the Oligo–Miocene in the South China Sea"*,
  Nature Communications, Volume 8, Article number: 15698, 2017,
  `DOI:10.1038/ncomms15698 <https://doi.org/10.1038/ncomms15698>`_

* Pérez-Ortiz A. & Borthwick G. L. A. & McNaughton J. & Avdis A.,
  *"Characterization of the tidal resource in Rathlin Sound"*,
  Renewable Energy, Volume 114, Part A, December 2017, Pages 229-243,
  `DOI:10.1016/j.renene.2017.04.026 <https://doi.org/10.1016/j.renene.2017.04.026>`_
