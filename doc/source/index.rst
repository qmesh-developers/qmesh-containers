.. This file is part of qmesh containers manual
   Copyright (C) 2019 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Contents
============

| Copyright (C) 2019 Alexandros Avdis & Jon Hill
|
| Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3  or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section titled "GNU  Free Documentation License".
|

Welcome to the qmesh containers manual.

qmesh is an unstructured triangular mesh generator for geophysical domains.
It is suited to mesh generation over topologically two-dimensional domains.
Such meshes are widely used in coastal and ocean modelling, with typical examples available in the :ref:`academic_papers` section.

qmesh was developed by `Alexandros Avdis <https://orcid.org/0000-0002-2695-3358>`_ and `Jon Hill <https://orcid.org/0000-0003-1340-4373>`_, primarily to meet our meshing needs.
We developed qmesh following an open-source philosophy, so you are welcome to use qmesh and access the source code, under the `GNU General Public License, version 3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_.
The license and development models are fully described in section :ref:`development_distribution_licence`.

As the project grew and matured, the code was reorganised into various modules and packages.
The aim of the present package is to facilitate development as well as use of qmesh.
We have opted for *Docker containers* as they offer completely isolated environments, thus facilitating the management of dependencies.
However, creating and managing Docker containers can itself be a demanding task.
Therefore, we created a utility, included in this project, for creating specialised Docker containers with all necessary dependencies installed.

.. toctree::
   :maxdepth: 3

   introduction
   installation
   tutorial
   developmentDistributionLicence
   furtherDocumentation
   references

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

