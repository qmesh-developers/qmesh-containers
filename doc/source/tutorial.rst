.. This file is part of qmesh containers manual
   Copyright (C) 2019 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _Tutorial:

Tutorial
==========================

Docker provides a user with a bare-bone system, and thus puts the task of system administration onto the container user.
Administering a Linux system can be demanding for users who just want to run qmesh, rather than spend time administering a system.
The ``qmeshcontainer`` utility will create an appropriate container.
The utility will pull a docker image with qmesh and all dependencies pre-installed, add you as a user with the appropriate user and group IDs, and run a container.
In this chapter we outline the functionality of the ``qmeshcontainer`` utility thorugh a few basic examples and provide a few guidelines on effective use of docker.
You must have installed the ``qmeshcontainer`` utility, as described in the :ref:`Installation` chapter.

Using the qmesh containers utility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a directory named ``qmeshcontainer_tutorial``.
Enter the directory and issue the following commands:

.. code-block:: bash

   $ qmeshcontainer -h

The command will display brief documentation on the full set of options of the ``qmeshcontainer`` utility, invoked by the ``-h`` flag:

.. literalinclude:: qmeshContainer_help.out
   :linenos:
   :language: console

To run a container with a qmesh installation issue the following command:

.. code-block:: bash

   $ qmeshcontainer -mwd

The rationale of containers is isolation, so when a container is built the files on the host are not available in the container.
However, it is occasionally useful to be able to work on host files, from inside a container.
Docker allows *mounting* host directories (or partitions) on the container, where particular directories on the host are accesible from inside the container.
The ``-mwd`` flag of the ``qmeshcontainer`` utility will mount your working directory (where you are running the script) in the container so that you can access data on the host system.
When mounting host volumes, it is essential to work in the container with care.
Any file changes and deletions in mounted volumes will affect files on the host system.

.. literalinclude:: qmeshContainer_example1.out
   :linenos:
   :language: console

The last line is the command prompt in your container.
The identifier ``username`` will be your username on the host machine, the ``qmeshcontainer`` utility obtains this as well as your user and group IDs on the host machine.
The reason for extracting this information is to make mounted directories accessible to both the host and container.
The alphanumeric ID `26af957e2df4` is the tag of your container and will be different to the above.
Try invoking the python interpreter and importing qmesh, as follows:

.. literalinclude:: qmeshContainer_example2.out
   :linenos:
   :language: console

Explore the other options of the ``qmeshcontainer`` utility.
In particular, the `-v` flag increases the verbosity of the script so that you are given more information while the container is being built.


Tiding up after Docker
~~~~~~~~~~~~~~~~~~~~~~~

Docker does have the disadvantage of consuming a lot of space on hard-drives and the high-level commands of the Docker deamon do not facilitate effective clean-up.
Should you start experiencing problems, where the docker command interface is not behaving as expected, the cause might be docker files filling up space (typically the partition mounted on ``/var``).
On a Linux system you can use the ``df`` command-line utility to gather further evidence and corroborate if one of your system partitions is filling up.
Assuming docker writes data to the partition mounted on ``/var``, the following steps will clean up your space:

.. code-block:: bash

   $ sudo service docker stop
   $ sudo rm -rf /var/lib/docker
   $ sudo service docker restart

However, this will delete all docker images on your system, so it is a good idea to save any images you want to keep.
Look into ``docker save`` and ``docker load`` commands for saving and loading containers (start with ``docker help save`` and so on).
It is a good idea to treat docker images as expendable and instead keep the docker-file used to build it, perhaps under version control.

.. Other tools for docker image management: https://github.com/justone/dockviz and sudo baobab /var/lib/docker
