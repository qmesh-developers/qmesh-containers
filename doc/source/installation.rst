.. This file is part of qmesh containers manual
   Copyright (C) 2019 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _Installation:

Installation
==========================

Dependencies
--------------------------

The dependencies of the qmesh containers utility are:

 * Docker.
 * Python. The qmesh containers utility is a python package. Python 2.7 and python 3 are supported.

Installing Docker
~~~~~~~~~~~~~~~~~~~~~~~

You must have docker installed on your system, the Docker Community Edition (CE) is sufficient.
We recommend using a package manager to install Docker on your system.
If you are new to Docker it is a good idea to first familiarise yourself with the `overview section in the Docker documentation pages <https://docs.docker.com/install/overview/>`_ .
Follow the instructions in the relevant web-pages, Docker is available on `MacOS <https://docs.docker.com/docker-for-mac/install/>`_ and `Windows <https://docs.docker.com/docker-for-windows/install/>`_ operating systems as well as several Linux distributions (`CentOS <https://docs.docker.com/install/linux/docker-ce/centos/>`_, `Debian <https://docs.docker.com/install/linux/docker-ce/debian/>`_, `Fedora <https://docs.docker.com/install/linux/docker-ce/fedora/>`_, `Ubuntu <https://docs.docker.com/install/linux/docker-ce/ubuntu/>`_), with `Linux binaries <https://docs.docker.com/install/linux/docker-ce/binaries/>`_ also available.

*If you are using Linux, you must also follow the* `Linux post-installation steps <https://docs.docker.com/install/linux/linux-postinstall/>`_.
The instruction will set-up your system for easier and more secure use of Docker.
In particular, make sure you create a ``docker`` group and add your (user) account to that group, as detailed in the post-installation instruction.

Installing from PyPI
--------------------------

The easiest way to install the qmesh containers utility is through the Python Package Index (PyPI) and the ``pip`` package manager.
We recommend installation through the ``pip`` package manager, although other approaches are possible.
``pip`` facilitates installation, upgrade and removal of python packages, across a range of access levels:
Given administrator (root) access, ``pip`` can install packages across a whole system.
In cases where security is a concern, user-level installations are possible.

System-wide installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Type the following at the command line:

.. code-block:: bash

   sudo pip install qmeshcontainers

Enter the appropriate credentials when prompted, to continue with installation.
The ``sudo`` utility is used above, as it is often available in many Linux and Unix systems.
It is a means of providing user accounts with administrator privileges.
Your system's set-up might require a different approach, for example using the ``su`` utility
to first create a root session and install the utility, without the ``sudo`` package: ``pip install qmeshcontainers``

Should you wish to upgrade, type the following at a terminal:

.. code-block:: bash

   sudo pip install --upgrade qmeshcontainers

Type the following to remove ``qmeshcontainers`` from your system:

.. code-block:: bash

   sudo pip uninstall qmeshcontainers

User-level installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Type the following at the command line:

.. code-block:: bash

   pip install --user qmeshcontainers

Should you wish to upgrade, type the following at a terminal:

.. code-block:: bash

   pip install --user --upgrade qmeshcontainers

Type the following to remove ``qmeshcontainers`` from your system:

.. code-block:: bash

   pip uninstall --user qmeshcontainers
