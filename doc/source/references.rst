.. This file id part of qmesh containers manual
   Copyright (C)  2019 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _References:

References
================

.. bibliography:: references.bib
   :cited:
   :style: unsrt

