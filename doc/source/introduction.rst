.. This file is part of qmesh containers manual
   Copyright (C) 2019 Alexandros Avdis.
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _Introduction:

Introduction
================

Containers, virtual machines, virtual environments and other similar methods are frequently used in the development of software, as they provide an isolated working environment.
By *working environment* we here mean an environment where software dependencies are already satisfied.
Therefore, the software in development can be immediately used and tested with minimal effort on installation.

.. _docker-containers:

Docker overview
--------------------------

A detailed description of Docker is beyond this manual, but for completeness we mention the most basic concepts related to Docker:

  * Docker Containers
  * Docker Images
  * Docker Engine
  * Docker Registries and repositories.
  * Docker image tags

*Docker containers* can be thought of as virtual machines. Containers provide isolation and control of a working environment.
*Docker images* are files that contain instructions on how to build a container.
The *Docker Engine* is the program that uses the instructions in an image to build and run containers.
Docker images can be distributed via *Docker Registries*.
The Docker Engine interfaces to registries, facilitating use of images.
For example, `Docker hub <https://hub.docker.com/>`_ is a popular docker registry, where developers upload images.
Images uploaded to registries are organised into *repositories*, typically each repository contains images pertinent to a project, package or organisation.
The `qmesh repository <https://hub.docker.com/r/qmesh/qmesh-containers/>`_ houses all qmesh images.
Each image is identified by a unique image *tag*, and typically the images in a repository are listed by their tags.

`The image tags in the qmesh repository <://hub.docker.com/r/qmesh/qmesh-containers/tags>`_ showcase the different roles of images in the development and distribution of software.
Some images are intended for testing and development. The environment is set-up with a minimal set of dependencies to provide a "sand-box".
Other images provide a full qmesh installation and are aimed at facilitating use of qmesh when a user's environment makes installation hard or impossible.
