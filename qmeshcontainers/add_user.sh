USERNAME=$1
USERID=$2
GROUPNAME=$3
GROUPID=$4
#Check if user-name and user-id are already in use
USERNAME_INUSE=`grep "\<$USERNAME\>:" /etc/passwd | wc -l`
if [ $USERNAME_INUSE -gt 0];then
	USERNAME_INUSE_UID=`grep ":\<$USERID\>:" /etc/passwd | cut -d ":" -f 3`
USERID_INUSE=`grep ":\<$USERID\>:" /etc/passwd | cut -d ":" -f 3`
#Check if group-name and group-id are already in use
GROUPNAME_INUSE=`grep "\<$GROUPNAME\>:" /etc/group | wc -l`
GROUPID_INUSE=`grep ":\<$GROUPID\>:" /etc/group | wc -l`
if [ $GROUPID_INUSE -gt 0 ];then
	if [ $GROUPNAME_INUSE -gt 0 ];then
